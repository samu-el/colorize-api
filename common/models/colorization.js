'use strict';
const exec = require('child_process').exec

module.exports = function(Colorization) {

    Colorization.auto = function (params, cb) {
        console.log('params', params);
        let image = './files/images/'+params;
        let color = './files/color2/'+params;
         new Promise((resolve, reject)=>{
                exec(`python ./colorization/colorize.py -i ${image} -o ${color} -m ./colorization/model.pth --gpu -1`, (err, stdout, stderr)=>{
                    if(err) reject(err)
                    else resolve('http://localhost:5000/api/Containers/color2/download/'+params)
                })
            })
            .then( (response) => {
                cb(null, response)
            })
            .catch( err => {
                cb(err)
            })
        
	}
    
    Colorization.remoteMethod('auto', {

		accepts: [
			{ "arg": "image", "type": "string" }
		],
		isStatic: true,
		returns: { arg: "colorImage", "type": "string" },
		http: { verb: 'post', path: '/auto' }
    });

};
